from strawberry import type, Schema, field
from typing import List



@type
class Book:
    title: str
    author: "Author" = field(resolver="get_author_for_book")


def get_author_for_book() -> "Author":
    return Author(name="Michael Crichton")


# books = [
# ]


def get_books_for_author() -> List["Book"]:
    return [Book(title="Jurassic Park"), Book(title="Deep work")]


def get_authors() -> List["Author"]:
    return [Author(name="Michael Crichton")]


@type
class Author:
    name: str
    books: List[Book] = field(resolver=get_books_for_author)


@type
class Query:
    books: List[Book] = field(resolver=get_books_for_author)
    authors: List[Author] = field(resolver=get_authors)


# @type
# class Mutation:
#     @field
#     def add_book(self, title: str, author: str) -> Book:


schema = Schema(query=Query)
